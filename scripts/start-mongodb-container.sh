#!/bin/sh


docker run --rm --network my-net --name some-mongo \
    -v db1:/data/db \
    -v configdb1:/data/configdb \
    -d mongo
